import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WhatWeDoComponent } from './components/what-we-do/what-we-do.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { OurWorkComponent } from './components/our-work/our-work.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    WhatWeDoComponent,
    ContactUsComponent,
    OurWorkComponent,
    NavbarComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
