import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WhatWeDoComponent } from './components/what-we-do/what-we-do.component';

const routes: Routes = 
  [
    { 
      path: '', component: WhatWeDoComponent,
    },
  ];

  
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
